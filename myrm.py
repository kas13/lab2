#!/usr/bin/env python

import sys
import logging
import argparse
import os
import json
import shutil
import datetime
import glob

from setuptools import setup, find_packages
from Load_config import Load_config

#to config


def _setup():
    try:
        os.mkdir("Basket")
    except:
        print("already done")


def _del_file(path_trash, path_information, path, dry_run=False, confirm=False):
    _check_confirm(confirm)
    pathStr = (os.path.abspath('_'.join(path)))
    (dirName, fileName) = os.path.split(pathStr)
    (fileBaseName, fileExtension) = os.path.splitext(fileName)
    try:
        if dry_run:
            print(pathStr + "  Was deleted")
        else:
            os.rename(pathStr, path_trash + fileName)
            _safe_file_Information(path_information, dirName, fileName)
    except():
        print("not found")


def _del_by_regex(regex, path_trash, path_information, dry_run=False, confirm=False):
    files = glob.glob(regex)
    print(files)


def _update_basket(path_trash, path_information):
    listdir = os.listdir(path_trash)
    print(listdir)
    file = open(path_information, "r")
    lines = file.readlines()
    file.close()
    for fileName in listdir:
        for line in lines:
            info = json.loads(line) #TODO


# raise SystemExit(-1)

def _restore_file(path_trash, path_information, name, confirm):
    _check_confirm(confirm)
    with open(path_information, "r") as json_file:
        lines = json_file.read()

    if not lines:
        config_info = json.loads(lines) if lines else []


    # list
    config_info = json.loads(lines)

    updated_config = []
    for config_item in config_info:
        # config_item = {"oldPath": "/home/student/Pyc/lab2", "fileName": "1"}
        if config_item['fileName'] == name:
            os.rename(path_trash + name, config_item['oldPath'] + '/' + name)
        else:
            updated_config.append(config_info)

    with open(path_information, 'w') as fp:
        json.dump(updated_config, fp)


def RestoreFile(path_trash, path_information, name, dry_run=False, confirm=False):
    # log.set_logger_level(log.INFO)
    # log.debug("Restoring file '{}'...".format(name))
    # import setuptools
    # import
    _check_confirm(confirm)
    file = open(path_information, "r")
    lines = file.readlines()
    file.close()
    with open(path_information, 'w') as fp:
        for line in lines:
            info = json.loads(line)
            if info['fileName'] == name:
                if dry_run is True:
                    print(path_trash + name + "  Was recovered")
                    json.dump(info, fp)
                    fp.write('\n')
                else:
                    os.rename(path_trash + name, info['oldPath'] + '/' + name)
            if info['fileName'] != name:
                json.dump(info, fp)
                fp.write('\n')


def _as_comlex(dct):
    if 'fileName' in dct:
        return complex(dct['oldPath'], dct['fileName'])
    return dct


def _safe_file_Information(path_information, oldPath, fileName):
    info = {}
    info['oldPath'] = oldPath
    info['fileName'] = fileName
    with open(path_information, 'a') as file:
        json.dump(info, file)
        file.write('\n')


def _get_size(path_trash):
    size = 0
    for d, dirs, files in os.walk(path_trash):
        for file in files:
            file_path = os.path.join(d, file)
            size += os.path.getsize(file_path)
    return size


def _clean_basket(path_trash, path_information, dry_run=False, confirm=False):
    _check_confirm(confirm)
    try:
        for file in os.listdir(path_trash):
            file_removed = os.path.join(path_trash, file)
            if os.path.isdir(file_removed):
                if dry_run is True:
                    print (shutil.rmtree, "was deleting")
                else:
                    shutil.rmtree(file_removed)
            else:
                if dry_run is True:
                    print (shutil.rmtree, "was deleting")
                else:
                    os.remove(file_removed)
        info_removed = open(path_information, "w")
        info_removed.close()

    except Exception:
        print(Exception)


def _automatic_clean(path_trash, path_information, day, size):
    if datetime.datetime.today().isoweekday() == day:
        _clean_basket(path_trash, path_information, False)
    if _get_size(path_trash) > size:
        _clean_basket(path_trash, path_information, False)


def _check_confirm(confirm):
    if confirm is True:
        answer = raw_input("Are you sure? y/n \n")
        if answer == "n":
            sys.exit("canceled")
        else:
            if answer != "y":
                _check_confirm(confirm)


def Main():
    path_trash = '/home/student/Pyc/lab2/Basket/'
    path_information = r'/home/student/Pyc/lab2/information.json'

    parser = argparse.ArgumentParser()
    parser.add_argument("Path", nargs='*', help="Delete file")
    parser.add_argument("-new", dest="setup", help="New Basket", action="store_true")
    parser.add_argument("-res", dest="restore", help="Restore")
    parser.add_argument("-u", dest="update", help="Update", action="store_true")
    parser.add_argument("-clean", dest="clean", help="Clean", action="store_true")
    parser.add_argument("-size", help="Size")
    parser.add_argument("-show", help="Show")
    parser.add_argument("-c", dest="confirm", help="confirm", action="store_true")
    parser.add_argument("-dry",  help="Dry Run", action="store_true")
    parser.add_argument("-reg", dest="regex", help="regex", action="store_true")
    args = parser.parse_args()

    dry_run = False
    confirm = False
    day = 3
    size = 100000
    config = Load_config()
    path_trash = config['trash_path']
    dry_run = config['dry']
    day = config['day']
    size = config['size']
    confirm = config['confirm']

    # to config


    if args.setup:
        _setup()
    if args.dry:
        dry_run = True
    if args.confirm:
        confirm = True
    if args.size:
        size = int(args.size)
    if args.Path:
        _del_file(path_trash, path_information, args.Path, dry_run, confirm)
    if args.restore:
        #_restore_file(path_trash, path_information, args.restore, confirm)
        RestoreFile(path_trash, path_information, args.restore, dry_run, confirm)
    if args.clean:
        _clean_basket(path_trash, path_information, dry_run, confirm)
    if args.update:
        _update_basket(path_trash, path_information)
    if args.regex:
        _del_by_regex(args.regex, path_trash, path_information, dry_run, confirm)
    if args.show:
        count = int(args.show)
        t_list = os.listdir(path_trash)[-count:]
        for f in t_list:
            print(f)

    _automatic_clean(path_trash, path_information, day, size)

if __name__ == '__main__':
    Main()
