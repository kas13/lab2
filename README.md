#DESCRIPTION
console program to delete files and directories, and restore them.  

#BASIC COMMAND
myrm - remove files or directories and restore removal objects out of the basket

#START GUIDE
put your settings in the file config.json or config.txt, then install the program:
sudo python setup.py install  

#FLAGS
myrm [path to file]  delete to basket

you can add arguments:  
1.  -rep      If a conflict occurs during the restoration of the file, the replacement will be made      
2.  -res      Restore file  
3.  -u        Update file information in basket  
4.  -clean    Clean basket  
5.  -a        returns the current basket size  
6.  -show     allows you to view the contents of the basket  
7.  -c        Confirm operation before performing the operation  
8.  -dry      only simulation, there is no change  
9.  -reg      Regular expression  
10. -log      log level  
11. -s        Silent mod, without output of additional information  
12. -size     basket size policy on which the basket will be automatically cleaned  
13. -day      basket day policy on which the basket will be automatically cleaned  
14. -hand     manual removal from the basket  
15. -config   Load config(config path, config format[json/txt])  

#CONFIG EXAMPLE JSON  

{  
  "trash_path": "/home/student/Pyc/lab2/Basket/",  
  "log_path": "/home/student/Pyc/lab2/files/log.log",  
  "information_path":"/home/student/Pyc/lab2/files/information.json",  
  "dry": false,  
  "day": "3",  
  "size": "100000",  
  "confirm": false,  
  "silent": false,  
  "replace_file": true,  
  "size_policy": false,  
  "day_policy": false,  
  "log_level": "DEBUG"  
}  
manual:    
"trash_path" - the path of your basket  
"log_path" - path to your logs    
"information_path" - path to information about the contents of the basket    
"dry" - simulation of operations, does not fulfill them    
"day" - automatic delete policy on certain days    
"size" - automatic removal policy for overflow of a certain size    
"confirm" - confirmation of an operation before execution    
"silent" - silent mode, execution of commands without outputting additional information    
"replace_file" - replacing files when conflicts occur    
"size_policy" - set an automatic deletion policy from the basket by size    
"day_policy" - set an automatic deletion policy from the basket by day  
"log_level" - logging level    



#MODULES
1.   myrm.py basic functions for the work of the basket, as well as for use in other programs as a library
2.   app.py  console application myrm
3.   load_config.py module for loading configs

#AUTOR
Kryzhanouski Aliaksandr