import json
import os

config_path = "/home/student/Pyc/lab2/files/config.json"

def Load_config(path = config_path):
    with open(config_path) as json_config:
        data = json.load(json_config)
    return data